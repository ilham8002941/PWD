<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Biodata</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css">
</head>
<body>
    <nav class="navbar navbar-expand-lg bg-body-tertiary">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">Navbar</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="index.html">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="biodata.html">Biodata</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="berita.html">Berita</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="galery.html">Galery</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Dropdown
                        </a>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="kontak.html">Kontak</a></li>
                            <li><a class="dropdown-item" href="tentang info">Tentang Info</a></li>
                            <li><hr class="dropdown-divider"></li>
                            <li><a class="dropdown-item" href="penerjemah">Penerjemah</a></li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled" aria-disabled="true">Disabled</a>
                    </li>
                </ul>
                <form class="d-flex" role="search">
                    <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success" type="submit">Search</button>
                </form>
            </div>
        </div>
    </nav>
    <div class="row">
        <div class="col-md-3 p-4">
            <div class="card">
                <div class="card-body">  
                    <img src="GH.jpg" class="img-fluid" alt="lukisan Digital">
                    
                </div>
            </div>
            
        </div>
        <div class="col-md-9 p-4">
            <div class="card">
                <div class="card-body bg-primary">
                    <table class="table table-hover table primary">
                        <thead>
                            <h1 class="text-center bg-light"><i>BIODATA DIRI</i></h1>
                            <table class="table table-hover table-warning">
                            </thead>
                            <tbody class="rtl">
                                <tr>
                                    <td>Nama</td>
                                    <td>:</td>
                                    <td>Ilham Taufikurahman</td>
                                </tr>
                                
                                <tr>
                                    <td>NPM</td>
                                    <td>:</td>
                                    <td>011220037</td>
                                </tr>
                                
                                <tr>
                                    <td>PRODI</td>
                                    <td>:</td>
                                    <td>INFORMATIKA</td>
                                </tr>
                                
                                <tr>
                                    <td>Tempat Tanggal Lahir</td>
                                    <td>:</td>
                                    <td>Banyuasin, 23 Juni 2004</td>
                                </tr>
                                
                                <tr>
                                    <td>Jenis Kelamin</td>
                                    <td>:</td>
                                    <td>Laki Laki</td>
                                </tr>
                                
                                <tr>
                                    <td>AGAMA</td>
                                    <td>:</td>
                                    <td>Islam</td>
                                </tr>
                                
                                <tr>
                                    <td>ALAMAT</td>
                                    <td>:</td>
                                    <td>Jl. Irigasi PAkjo</td>
                                </tr>
                                
                                <tr>
                                    <td>Pekerjaan</td>
                                    <td>:</td>
                                    <td>Mahasiswa</td>
                                </tr>
                                
                                <tr>
                                    <td>Kewarganegaraan</td>
                                    <td>:</td>
                                    <td>Indonesia</td>
                                </tr>
                                
                                <tr>
                                    <td>No. Tlp</td>
                                    <td>:</td>
                                    <td>0822-6985-9060</td>
                                </tr>
                                
                                <tr>
                                    <td>Hobi</td>
                                    <td>:</td>
                                    <td>
                                        <ul>
                                            <li>Digital Art</li>
                                            <li>Catur</li>
                                            <li>Voli</li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Riwayat Pendidikan</td>
                                    <td>:</td>
                                    <td>
                                        <ol>
                                            <li>SDN 28 Pulau Rimau</li>
                                            <li>SMP Negri 2 Pulau Rimau</li>
                                            <li>MA Darul Ulum Pulau Rimau</li>
                                        </ol>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    
    
</body>
</html>